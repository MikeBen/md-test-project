- Cat
- Dog
- Turtle

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```


- {+ addition 1 +}
- [+ addition 2 +]
- {- deletion 3 -}
- [- deletion 4 -]


| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |